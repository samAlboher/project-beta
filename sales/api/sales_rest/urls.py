from django.urls import path
from .views import (api_employees, api_employee, api_customers, api_customer, api_sales, api_employee_sales)
api_employees
urlpatterns = [
    path("employees/", api_employees, name="api_employees"),
    path("employees/<str:pk>/", api_employee, name="api_employee"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:pk>/", api_employee_sales, name="api_employee_sales"),
]
