from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    EmployeeEncoder,
    CustomerEncoder,
    SalesEncoder,
)
from .models import SalesPerson, SalesRecord, PotentialCustomer, AutomobileVO


@require_http_methods(["GET", "POST"])
def api_employees(request):
    if request.method == "GET":
        employees = SalesPerson.objects.all()
        return JsonResponse({"employees": employees}, encoder=EmployeeEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            employee = SalesPerson.objects.create(**content)
            return JsonResponse(employee, encoder=EmployeeEncoder, safe=False,)
        except:
            response = JsonResponse({"message": "Could not create the employee"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE", "PUT"])
def api_employee(request, pk):
    if request.method == "GET":
        try:
            employee = SalesPerson.objects.get(employee_number=pk)
            return JsonResponse(employee, encoder=EmployeeEncoder, safe=False,)
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Employee does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            employee = SalesPerson.objects.get(employee_number=pk)
            employee.delete()
            return JsonResponse({"Deleted": employee}, encoder=EmployeeEncoder, safe=False,)
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            employee = SalesPerson.objects.get(employee_number=pk)

            props = ['employee_name']
            for prop in props:
                if prop in content:
                    setattr(employee, prop, content[prop])
            employee.save()
            return JsonResponse(employee, encoder=EmployeeEncoder, safe=False,)
        except:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False,)
        except:
            response = JsonResponse({"message": "Could not create the customer"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = PotentialCustomer.objects.get(id=pk)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False,)
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = PotentialCustomer.objects.get(id=pk)
            customer.delete()
            return JsonResponse({"Deleted": customer}, encoder=CustomerEncoder, safe=False,)
        except PotentialCustomer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.get(id=pk)

            props = ['name', 'address', 'phone']
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False,)
        except:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        try:
            sales = SalesRecord.objects.all()
            return JsonResponse({"sales": sales}, encoder=SalesEncoder
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"sales": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            employee_number = content['sales_person']
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            content['sales_person'] = sales_person
            customer = content['customer']
            customer = PotentialCustomer.objects.get(id=customer)
            content['customer'] = customer
            vin = content['auto']
            vin = AutomobileVO.objects.get(vin=vin)
            content['auto'] = vin
            sale = SalesRecord.objects.create(**content)
            return JsonResponse(sale, encoder=SalesEncoder, safe=False,)
        except:
            response = JsonResponse({"message": "Could not create the Sale"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE","PUT"])
def api_employee_sales(request, pk):
    if request.method == "GET":
        try:
            sale = SalesRecord.objects.get(pk=pk)
            return JsonResponse(sale, encoder=SalesEncoder, safe=False)
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = SalesRecord.objects.get(pk=pk)
            sale.delete()
            return JsonResponse({"Deleted": sale}, encoder=SalesEncoder, safe=False,)
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        sale = SalesRecord.objects.get(pk=pk)
        print(sale)
        props = ['sale_price']
        for prop in props:
            if prop in content:
                setattr(sale, prop, content[prop])
        sale.save()
        return JsonResponse(sale, encoder=SalesEncoder, safe=False,)
