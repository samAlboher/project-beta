from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    employee_name = models.CharField(max_length=25)
    employee_number = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_employee", kwargs={"pk": self.employee_number})

class PotentialCustomer(models.Model):
    name = models.CharField(max_length=25)
    address = models.CharField(max_length=200, unique=True)
    phone = models.PositiveBigIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

class SalesRecord(models.Model):
    sale_price = models.PositiveIntegerField()
    sales_person = models.ForeignKey(
        SalesPerson,
        on_delete=models.CASCADE
        )
    customer = models.ForeignKey(
        PotentialCustomer,
        on_delete=models.CASCADE
        )
    auto = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE
        )

    def get_api_url(self):
        return reverse("api_employee_sales", kwargs={"pk": self.id})
