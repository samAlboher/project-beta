from common.json import ModelEncoder

from .models import SalesPerson, SalesRecord, PotentialCustomer, AutomobileVO

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class EmployeeEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "employee_name",
        "employee_number",
    ]


class CustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "id",
        "name",
        "address",
        "phone",
    ]


class SalesEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "pk",
        "sale_price",
        "sales_person",
        "customer",
        "auto",
    ]
    encoders = {
        "sales_person": EmployeeEncoder(),
        "customer": CustomerEncoder(),
        "auto": AutomobileVOEncoder(),
    }
