from django.db import models
from django.urls import reverse
from django.utils import timezone
from datetime import date

# Create your models here.
class AutomobileVO(models.Model):
    '''
    polls vin from inventory microservice
    '''
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    '''
    Stors technician details
    linked to appointments(foreign key appointment)
    '''
    employee_name = models.CharField(max_length=200, unique=True)
    employee_number= models.CharField(max_length=200, unique=True)
    def get_api_url(self):
        return reverse("api_show_technicians", kwargs={"pk": self.id})
"""
create/update technician insomnia template
{
    "employee_name": "",
    "employee_number": ""
}
"""


class Appointment(models.Model):
    '''
    stores appointment details
    used for history of appointments
    linked to automobile in inventory
    '''

    date = models.DateTimeField()
    reason = models.TextField(max_length=300)
    customer_name = models.CharField(max_length=200)
    completed = models.BooleanField(default=False, null=True)
    vin = models.ForeignKey(
        AutomobileVO,
        related_name='automobile',
        on_delete=models.CASCADE
    )
    technician = models.ForeignKey(
        Technician,
        related_name='appointment',
        on_delete=models.CASCADE
    )
    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"pk": self.pk})
"""
create/update insomnia template

{
    "date": "",
    "time": "",
    "reason": "",
    "customer_name": "",
    "completed": "",
    "vin": "",
    "technician": ""

}

"""
