from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Appointment, Technician, AutomobileVO
from .encoders import AppointmentEncoder, TechnicianEncoder


# Create your views here.


"""APPOINTMENTS"""


@require_http_methods(["GET", "POST"])
def api_list_appointments(request,):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        print((content['vin']))
        try:
            employee = content['technician']
            technician = Technician.objects.get(employee_number=employee)
            auto = content['vin']
            vin = AutomobileVO.objects.get(vin=auto)
            content['technician'] = technician
            content['vin'] = vin
        except:
            if AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"error": "Vin not found"}, status=400,)
            return JsonResponse(
                {"error": "Techinician does not exist"}, status=400,)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment Does not exist"}, status=400
            )
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            print("content", content)
            print('pk',pk)
            appointment = Appointment.objects.get(id=pk)

            props = ["date", "time", "reason", "customer_name","completed", "vin", "technician"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})



"""TECHNICIANS"""


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT","DELETE"])
def api_show_technician(request,pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician Does not exist"}, status=400
            )
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(pk=id)

            props = ["employee_name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"})
