from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
import json
# from datetime import strfdate


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "id",
        ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_name",
        "employee_number"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date",
        # "time",
        "reason",
        "customer_name",
        "completed",
        "vin",
        "technician",
    ]
    encoders = {
            "vin": AutomobileVOEncoder(),
            "technician": TechnicianEncoder(),
        }
    # def get_extra_data(self, o):
    #     return {"date":json.dumps(o.date), "time": json.dumps(o.time)}
