from django.urls import path
from .views import api_show_appointment, api_list_appointments, api_show_technician, api_list_technicians


urlpatterns = [
    #'''APPOINTMENTS URLS'''
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("appointments/", api_list_appointments, name="appointments"),
    #'''TECHNICIAN URLS'''
    path("technicians/<int:pk>/", api_show_technician, name="api_show_technician"),
    path("technicians/", api_list_technicians, name="technicians"),

]
