# Automotive Management Systems

Team:

* Sam - Service
* Triston - Sales

## Design

## Service microservice
MODELS:
To achieve exceptional functionality and satisfy the clients requirements, we have implemented models
which propel appointment and technicina creation. Our appointment model employs polling the inventory microservice for vins used in our appointments. Furthermore our appointment model assigns technicians to appointments using djangos foreign keys.

URl paths:
Url paths use a CRUD schematic keeping url paths predictible and clean.

VIEWS:
Expounding upon URl paths, our views provide and accept json encoded or translated information necessary to communicate between our Postgres database and user front end.

REACT Front-End:
User interaction on the front end is provided through React functional components. Employing series of hooks to manage state changes.

UX:
User experience is improved through the use of toggling features in employee creation. By toggling jsx elements along with url pathways the same form may be used for tehnician or salesperson creation, reducing load times as well as improving cleanliness of code on the back end.

Similar toggling is used within application creation form and applications in progress list.

Appointment in progress list encoporates onClick functionality for "completed" button provided to remove completed applications from in progress list. These appointments can be viewed in appointment history. onClick functionality for "cancel" is provided to remove an appointment from the database. VIP indication is provided with an asterisk attached to customer name tied to vins which were in inventory. Currently vins available for appointment creation are tied to an automobileVO its data through polling. in future applications we suggest breaking this foreign key relationship to allow for the creation of appointments for vins outside of inventory giving more power to vip functionality.

Within appointment history list functionality is increased through search bar impletmentation allowing users to sift through appointments based on vin.


## Sales microservice


Models:
    Created Four models that represent VIN, Salesperson, Customer, and Sales. The VIN is a value object from the inventory models Automobile. I used polling to achieve this. Salesperson has two variables, employee name and employee number. Customer has three variables, name, address and phone. Sales has three foreign key variables, salesperson(Salesperson), auto(VIN), and customer(Customer), the last variable is it's sales price.
Url:
    The urls are RESTful. The endpoints clearly state where the information is being stored and where to retrieve that information.
View:
    The views fully implements CRUD method. The views encode or decode on which request is made, making the data flow uninterrupted. If the data flow is interrupted, we implemented errors to be shown to the user.
React:
    User interaction on the front end is provided through React functional components. Employing series of hooks to manage state changes.
