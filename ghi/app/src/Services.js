import {useState, useEffect} from 'react';
import { format, parseISO } from 'date-fns';


const initialData = {
    date: "",
    reason: "",
    customer_name: "",
    completed: "",
    vin: "",
    technician: "",

}

const NewServiceForm = () =>{
    const [formData, setFormData] = useState(initialData);
    const [autos, setAutoData] = useState([]);
    const [technicians, setTechnician] = useState([]);
    const [appointments, setAppointment] = useState([]);
    let vins = [];


    const getTechnicians = async () => {
        const url = await fetch('http://localhost:8080/api/technicians/')
        const data = await url.json();
        setTechnician(data.technicians)

    }

    const getAutos = async() =>{
        const url = await fetch(`http://localhost:8100/api/automobiles/`);
        const data = await url.json()
        setAutoData(data.autos)

    }

    const getAppointments = async() =>{
        const url = await fetch(`http://localhost:8080/api/appointments/`);
        const data = await url.json()
        setAppointment(data.appointments)
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleComplete = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const fetchOptions = {
            method: 'put',
            body: JSON.stringify({completed: true}),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(url, fetchOptions)
        if (resp.ok){

        }
        getAppointments()
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (vins.includes(formData.vin)){
            formData.customer_name += "*"
        }
        const url = `http://localhost:8080/api/appointments/`;
        delete formData.auto
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
            handleClick()
        }
        getAppointments()
    }

    useEffect( () =>{
        getAutos();
        getAppointments();
        getTechnicians();
    },[])

    const handleDelete = async (appointment) => {
        const url = await fetch(`http://localhost:8080/api/appointments/${appointment}/`, {method: 'DELETE'});
        getAppointments()
    }

    const [toggle, setToggle] = useState(true)

    const handleClick = () =>{
        setToggle(!toggle)
    }

    let hideForm = '';
    let aptList = 'd-none'
    if  (toggle === true) {
        hideForm = 'd-none';
        aptList = '';
    }

    return (
        <div className="row">
        <div className=" col-60">
            <div className="shadow p-4 mt-4">
                <div className={hideForm}>
                    <h1>Add New appointment!</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="mb-3">
                      <select onChange={handleChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-select"> */}
                         <option value="">Vin</option>
                         {autos.map(auto => {vins.push(auto.vin)
                             return(
                             <option key={auto.vin} >{auto.vin}</option>
                             );
                        })}
                     </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.customer_name} placeholder="Customer" required type="text" name="customer_name" id="customer_name" className="form-control" />
                            <label htmlFor="customer_name">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.date} placeholder="Date" required type="text" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a Technician</option>
                                {technicians.map( technician => {
                                    return (
                                    <option key={technician.employee_number} value={technician.employee_number}>
                                        {technician.employee_name}
                                    </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="row row-cols-2">
                        <button  className="btn btn-primary pe-2" >Schedule a Service</button>
                        <button  className="btn btn-secondary ps-2" onClick={handleClick} >My Appointments!</button>
                        </div>
                    </form>
                </div>
                    <div className={aptList}>
                        <h1>Appointments in Progress</h1>
                        <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Vin</th>
                                        <th>Customer Name</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Technician</th>
                                        <th>Reason</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {appointments.filter((appointment) => appointment.completed !== true).map(appointment => {
                                    return (
                                    <tr key={appointment.id}>
                                        <td>{ appointment.vin.vin }</td>
                                        <td>{ appointment.customer_name }</td>
                                        <td>{ format(parseISO(appointment.date), "PP") }</td>
                                        <td>{ format(parseISO(appointment.date), "p") } </td>
                                        <td>{ appointment.technician.employee_name }</td>
                                        <td>{ appointment.reason }</td>
                                        <td><button className="btn btn-success" onClick={() =>{handleComplete(appointment.id)}} name="completed" >Completed</button></td>
                                        <td><button className="btn btn-danger" onClick={() => {handleDelete(appointment.id)}}>Cancel</button></td>
                                    </tr>
                                    );
                                })}
                                </tbody>
                        </table>
                        <button className="btn btn-primary ps-2" onClick={handleClick}>Add Another Appointment</button>
                        <a href="/servicehistory"><button className="btn btn-info ps-2">Appointment History</button></a>
                        <div>Asterisk indicates VIP status</div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewServiceForm
