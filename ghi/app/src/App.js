import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Manufacturers from './ManuForm';
import Models from './ModelForm';
import Automobiles from './MobileForm';
import Services from './Services';
import ServiceHistory from './ServiceHistory';
import NewSalesPerson from './EmployeeForm';
import CustomerForm from './CustomerForm';
import Sales from './Sales';
import SalesForm from './SalesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<Manufacturers />} />
          <Route path="/vehicles" element={<Models />} />
          <Route path="/automobiles" element={<Automobiles />} />
          <Route path="/services" element={<Services />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />
          <Route path="/employees/new" element={<NewSalesPerson />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/new/sale/" element={<SalesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
