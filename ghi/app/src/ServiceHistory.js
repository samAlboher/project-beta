import {useState, useEffect} from 'react';
import { format, parseISO } from 'date-fns';



const SearchBar = () =>{
    const [searchInput, setSearchInput] = useState("");
    const [appointments, setAppointment] = useState([]);
    const [searchItem, setSearchItem] = useState([])

    const handleChange = (e) =>{
        e.preventDefault();
        setSearchInput(e.target.value);
    };

    const handleClick = () => {
        setSearchItem(appointments.filter((appointment) => appointment.vin['vin'].includes(searchInput)));

    }
    const handleClick2 = () => {
        setSearchItem(appointments.slice());
    }
    useEffect(()=>{
    const getAppointments = async() =>{
        const url = await fetch(`http://localhost:8080/api/appointments/`);
        const data = await url.json()
        setAppointment(data.appointments)
        setSearchItem(data.appointments)
        }
        getAppointments()
    },[])


    return(
        <div>
            <input type="text" placeholder="Search here" onChange={handleChange} value={searchInput} />
            <button onClick={handleClick}>search</button>
            <button onClick={handleClick2}>All Appointments</button>
                <div>
                <h1>Appointments History</h1>
                    <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Vin</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                        <tbody>
                            {searchItem.map((appointment) => {
                            return(
                            <tr key={appointment.id}>
                                <td>{ appointment.vin.vin }</td>
                                <td>{ appointment.customer_name }</td>
                                <td>{ format(parseISO(appointment.date), "PP") }</td>
                                <td>{ format(parseISO(appointment.date), "p") } </td>
                                <td>{ appointment.technician.employee_name }</td>
                                <td>{ appointment.reason }</td>
                            </tr>
                            );
                        })}
                        </tbody>
                    </table>
                    <a href="/services"><button className="btn btn-warning ps-2">Appointments in Progress</button></a>
                    <div>Asterisk indicates VIP status</div>
                </div>
            </div>
    )
}
export default SearchBar
