import {useState, useEffect} from 'react';

let initialData = {
    "name": "",
}
const Manufacturers = () =>{
    const [formData, setFormData] = useState(initialData);
    const [manufacturers, setManufacturer] = useState([]);

    const getManufacturers = async () => {
        const url = await fetch('http://localhost:8100/api/manufacturers/')
        const data = await url.json();
        setManufacturer(data.manufacturers)

    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `http://localhost:8100/api/manufacturers/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
            handleClick();
        }
        getManufacturers();
    }

    useEffect( () =>{
        getManufacturers();
    },[])

    const [toggle, setToggle] = useState(true);

    const handleClick = () =>{
        setToggle(!toggle)
    }


    let hideForm = '';
    let manuList = 'form-select d-none';
    if  (toggle === true) {
        hideForm = 'd-none';
        manuList = '';
    }

    return (

    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className={hideForm}>
            <h1>Add Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="row row-cols-2">
                <button  className="btn btn-primary pe-2" >Add Manufacturer</button>
                <button  className="btn btn-secondary ps-2" onClick={handleClick}>Manufacturers</button>
                </div>
            </form>
            </div>
            <div className={manuList}>
              <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                    return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <button className="btn btn-secondary ps-2" onClick={handleClick}>Add Manufacturer</button>
            </div>
            </div>
        </div>
    </div>

  )
}

export default Manufacturers;
