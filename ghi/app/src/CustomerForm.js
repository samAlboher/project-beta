import { useState } from "react";

let initialData = {
    "name": "",
    "address": "",
    "phone": "",
}

const CustomerForm = () =>{
    const [formData, setFormData] = useState(initialData);

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `http://localhost:8090/api/customers/`;

        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>New Customer</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.phone} placeholder="Phone Number" required type="text" name="phone" id="phone" className="form-control" />
                            <label htmlFor="phone">Phone Number</label>
                        </div>
                        <button  className="btn btn-primary" >Create Customer</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
