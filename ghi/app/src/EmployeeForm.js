import { useState } from "react";

let initialData = {
    "employee_name": "",
    "employee_number": "",
}

const NewSalesPerson = () =>{
    const [formData, setFormData] = useState(initialData);

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }
    const [toggle, setToggle] = useState(true)

    const handleClick = () =>{
        setToggle(!toggle)
    }
    const toggleSubmit = async (e) => {
        e.preventDefault();
        let url;
        if (toggle){
            url = `http://localhost:8090/api/employees/`;
        }
        else{
            url = `http://localhost:8080/api/technicians/`
        }
        console.log(formData)


        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
        }
    }



    let title = "Salesperson"
    let choice = "Technician"
    if (toggle === false){
        title = "Technician"
        choice = "Salesperson"
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>New {title}</h1>
                    <form  onSubmit={toggleSubmit} id="create-location-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.employee_name} placeholder="Name" required type="text" name="employee_name" id="employee_name" className="form-control" />
                            <label htmlFor="employee_name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.employee_number} placeholder="ID Number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">ID Number</label>
                        </div>
                        <button  className="btn btn-primary" >Create Employee</button>
                        <button  className="btn btn-secondary" onClick={handleClick}>Create {choice}</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default NewSalesPerson;
