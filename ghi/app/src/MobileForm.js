import {useState, useEffect} from 'react';

let initialData = {
    "color": "",
    "year": "",
    "vin": "",
    "model_id": "",

}
const Automobiles = () =>{
    const [formData, setFormData] = useState(initialData);
    const [automobiles, setAutomobiles] = useState([]);
    const [models, setModels] = useState([]);

    const getAutomobiles = async () => {
        const url = await fetch('http://localhost:8100/api/automobiles/')
        const data = await url.json();
        setAutomobiles(data.autos)

    }

    const getModels = async () => {
        const url = await fetch('http://localhost:8100/api/models/')
        const data = await url.json();
        setModels(data.models)
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `http://localhost:8100/api/automobiles/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
            handleClick();
        }
        getAutomobiles();
    }

    useEffect( () =>{
        getModels();
        getAutomobiles();
    },[])

    const [toggle, setToggle] = useState(true);

    const handleClick = () =>{
        setToggle(!toggle)
    }


    let hideForm = '';
    let autoList = 'form-select d-none';
    if  (toggle === true) {
        hideForm = 'd-none';
        autoList = '';
    }

    return (

    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className={hideForm}>
            <h1>Add Model</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.name} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                        <option value="">Choose a Model</option>
                        {models.map( model => {
                            return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                            )
                        })}
                    </select>
                </div>
                <div className="row row-cols-2">
                <button  className="btn btn-primary pe-2" >Add Automobile</button>
                <button  className="btn btn-secondary ps-2" onClick={handleClick}>Automobile</button>
                </div>
            </form>
            </div>
            <div className={autoList}>
              <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
                <tbody>
                    {automobiles.map(automobile => {
                    return (
                    <tr key={automobile.vin}>
                        <td>{ automobile.vin }</td>
                        <td>{ automobile.color }</td>
                        <td>{ automobile.year }</td>
                        <td>{ automobile.model.name }</td>
                        <td>{ automobile.model.manufacturer.name }</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <button className="btn btn-secondary ps-2" onClick={handleClick}>Add Automobile</button>
            </div>
            </div>
        </div>
    </div>

  )
}
export default Automobiles;
