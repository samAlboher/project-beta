function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Automotive Management Services</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Where premium service and sales happen!
        </p>
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRsEUxIgwcaoHuQWZYZcB2yLBlv6S-UrgHuNQ&usqp=CAU"/>
      </div>
    </div>
  );
}

export default MainPage;
