import { useState, useEffect } from "react";


const Sales = () =>{
    const [sales, setSaleData] = useState([]);
    const [employees, setEmployeeData] = useState([]);
    const [employeeNumber, setEmployeeNumber] = useState('');

    const getSales = async() =>{
        const url = await fetch(`http://localhost:8090/api/sales/`);
        const data = await url.json()
        setSaleData(data.sales)

    }

    const getSalesPerson = async() =>{
        const url = await fetch(`http://localhost:8090/api/employees/`);
        const data = await url.json()
        setEmployeeData(data.employees)

    }

    const handleChange = (e) => {
        setEmployeeNumber(e.target.value)
    }

    useEffect(() =>{
        getSales();
        getSalesPerson();
    }, [])


    return (
        <div className="container">
            <h1>Sales Person History</h1>
                    <div >
                    <select onChange={handleChange} placeholder="Sales Person" required type="text" name="sales_person" id="sales_person" className="form-select">
                        <option value="">Sales Person</option>
                        {employees.map(employee => {
                        return(
                        <option key={employee.employee_number} value={employee.employee_number}>{employee.employee_name}</option>
                        );
                    })}
                    </select>
                    <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sales price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {sales.map( sale => { if(sale.sales_person.employee_number === employeeNumber || employeeNumber === '')
                    return(
                    <tr key={sale.id}>
                        <td>{ sale.sales_person.employee_name }</td>
                        <td>{ sale.customer.name }</td>
                        <td>{ sale.auto.vin }</td>
                        <td >${ sale.sale_price.toLocaleString() }</td>
                    </tr>
                        )})}
                    </tbody>
                    </table>
                    </div>

    </div>
    );
}
export default Sales;
