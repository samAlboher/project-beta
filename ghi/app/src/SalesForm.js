import { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';


let initialData = {
    "sale_price": "",
    "sales_person": "",
    "customer": "",
    "auto": "",
}

const SalesForm = () =>{
    const [formData, setFormData] = useState(initialData);
    const [automobiles, setAutoData] = useState([]);
    const [employees, setEmployeeData] = useState([]);
    const [customers, setCustomerData] = useState([]);


    const getAuto = async() =>{
        const url = await fetch(`http://localhost:8100/api/automobiles/`);
        const data = await url.json()
        setAutoData(data.autos)

    }

    const getSalesPerson = async () => {
        const url = await fetch('http://localhost:8090/api/employees/')
        const data = await url.json();
        setEmployeeData(data.employees)

    }

    const getCustomers = async() =>{
        const url = await fetch(`http://localhost:8090/api/customers/`);
        const data = await url.json()
        setCustomerData(data.customers)

    }

    useEffect( () =>{
        getAuto();
        getSalesPerson();
        getCustomers();
    },[])

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `http://localhost:8090/api/sales/`;
        const autoUrl = await fetch(`http://localhost:8100/api/automobiles/${formData.auto}`, {method: 'DELETE'});

        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
            navigate('/sales');
        }
        else{
            console.log(resp.status);
        }
        getAuto();

    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>New Sale</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                       <div className="mb-3">
                            <select onChange={handleChange} value={formData.auto} placeholder="Automobile" required type="text" name="auto" id="auto" className="form-select">
                                <option value="">Automobile</option>
                                {automobiles.map(automobile => {
                                    return(
                                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange} value={formData.sales_person} placeholder="Sales Person" required type="text" name="sales_person" id="sales_person" className="form-select">
                                <option value="">Sales Person</option>
                                {employees.map(employee => {
                                    return(
                                    <option key={employee.employee_number} value={employee.employee_number}>{employee.employee_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChange} value={formData.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-select">
                                <option value="">Customer</option>
                                {customers.map(customer => {
                                    return(
                                    <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} value={formData.sale_price} placeholder="Price" required type="text" name="sale_price" id="sale_price" className="form-control" />
                            <label htmlFor="sale_price">Price</label>
                        </div>
                        <button  className="btn btn-primary" >Create Customer</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
