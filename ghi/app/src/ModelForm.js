import {useState, useEffect} from 'react';

let initialData = {
    "name": "",
    "picture_url": "",
    "manufacturer_id": "",
}
const Models = () =>{
    const [formData, setFormData] = useState(initialData);
    const [manufacturers, setManufacturer] = useState([]);
    const [models, setModels] = useState([]);

    const getManufacturers = async () => {
        const url = await fetch('http://localhost:8100/api/manufacturers/')
        const data = await url.json();
        setManufacturer(data.manufacturers)

    }

    const getModels = async () => {
        const url = await fetch('http://localhost:8100/api/models/')
        const data = await url.json();
        setModels(data.models)
    }

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `http://localhost:8100/api/models/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(url, fetchOptions);
        if (resp.ok) {
            setFormData(initialData);
            handleClick();
        }
        getModels();
    }

    useEffect( () =>{
        getModels();
        getManufacturers();
    },[])

    const [toggle, setToggle] = useState(true);

    const handleClick = () =>{
        setToggle(!toggle)
    }


    let hideForm = '';
    let modelList = 'form-select d-none';
    if  (toggle === true) {
        hideForm = 'd-none';
        modelList = '';
    }

    return (

    <div className="row">
    <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className={hideForm}>
            <h1>Add Model</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                        <option value="">Choose a Manufacturer</option>
                        {manufacturers.map( manufacturer => {
                            return (
                            <option key={manufacturer.id} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            )
                        })}
                    </select>
                </div>
                <div className="row row-cols-2">
                <button  className="btn btn-primary pe-2" >Add Model</button>
                <button  className="btn btn-secondary ps-2" onClick={handleClick}>Models</button>
                </div>
            </form>
            </div>
            <div className={modelList}>
              <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
                <tbody>
                    {models.map(model => {
                    return (
                    <tr key={model.id}>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name }</td>
                        <td><img src={ model.picture_url }/></td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            <button className="btn btn-secondary ps-2" onClick={handleClick}>Add Model</button>
            </div>
            </div>
        </div>
    </div>

  )
}
export default Models;
